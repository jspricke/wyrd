(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)

val prefix     : string
val sysconfdir : string
val version    : string
