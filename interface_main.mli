(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)


val create_windows : Curses.window -> Interface.screen_t

val append_quick_event : string -> string -> Time_lang.event_t * string

val run : Interface.interface_state_t -> unit
