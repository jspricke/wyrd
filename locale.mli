(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *  SPDX-License-Identifier: GPL-2.0-only
 *)

(* OCaml binding for setlocale(), required to kick ncurses into
 * properly rendering non-ASCII chars. *)

type t = LC_ALL | LC_COLLATE | LC_CTYPE | LC_MONETARY |
         LC_NUMERIC | LC_TIME | LC_MESSAGES |
         LC_UNDEFINED of int

external setlocale_int : int -> string -> string option = "ml_setlocale"

val setlocale : t -> string -> string option
