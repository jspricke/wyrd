{0 Wyrd Configuration File}

{1:advanced Advanced Configuration}

Wyrd reads a run-configuration textfile (generally [/etc/wyrdrc] or [/usr/local/etc/wyrdrc]) to determine key bindings, color schemes, and many other settings.
You can create a personalized configuration file in [$HOME/.wyrdrc], and select settings that match your usage patterns.
The recommended procedure is to "include" the [wyrdrc] file provided with Wyrd (see {{!include} Including Other Rcfiles}), and add or remove settings as desired.

{1 [wyrdrc] Syntax}

You may notice that the [wyrdrc] syntax is similar to the syntax used in the configuration file for the Mutt email client (muttrc).

Within the [wyrdrc] file, strings should be enclosed in double quotes (["]).
A double quote character inside a string may be represented by [\"].
The backslash character must be represented by doubling it ([ \\ ]).

{1:include Including Other Rcfiles}
Syntax: include {e filename_string}

This syntax can be used to include one run-configuration file within another.
This command could be used to load the default [wyrdrc] file (probably found in [/etc/wyrdrc] or [/usr/local/etc/wyrdrc]) within your personalized rcfile, [~/.wyrdrc].
The filename string should be enclosed in quotes.

{1:setvar Setting Configuration Variables}
Syntax: set {e variable}={e value_string}

A number of configuration variables can be set using this syntax; check the {{!variables} Configuration Variables} description to see a list.
The variables are unquoted, but the values should be quoted strings.

{1:bindings Creating Key Bindings}
Syntax: bind {e key_identifier operation}

This command will bind a keypress to execute a calendar operation.
The various operations, which should not be enclosed in quotes, may be found in the section on {{!operationslist} Calendar Operations}.

Key identifiers may be specified by strings that represent a single keypress, for example ["m"] (quotes included).
The key may be prefixed with ["\\C"] or ["\\M"] to represent Control or Meta (Alt) modifiers, respectively; note that the backslash must be doubled.
A number of special keys lack single-character representations, so the following strings may be used to represent them:

- ["<esc>"]
- ["<tab>"]
- ["<enter>"]
- ["<return>"]
- ["<insert>"]
- ["<home>"]
- ["<end>"]
- ["<pageup>"]
- ["<pagedown>"]
- ["<space>"]
- ["<left>"]
- ["<right>"]
- ["<up>"]
- ["<down>"]
- ["<f1>"] to ["<f12>"]

Due to differences between various terminal emulators, this key identifier syntax may not be adequate to describe every keypress.
As a workaround, Wyrd will also accept key identifiers in octal notation.
As an example, you could use [\024] (do {e not} enclose it in quotes) to represent Ctrl-T.

Multiple keys may be bound to the same operation, if desired.


{1:unbindings Removing Key Bindings}
Syntax: unbind {e key_identifier}

This command will remove all bindings associated with the key identifier.
The key identifiers should be defined using the syntax described in the previous section.


{1:colors Setting the Color Scheme}
Syntax: color {e object} {e foreground} {e background}

This command will apply the specified foreground and background colors to the appropriate object.
A list of colorable objects is provided in the section on {{!colorable_objects} Colorable Objects}.

Wyrd will recognize the following color keywords: [black], [red], [green], [yellow], [blue], [magenta], [cyan], [white], [default].
The [default] keyword allows you to choose the default foreground or background colors.
If you use [default] for your background color, this will access the transparent background on terminal emulators which support it.


{1:variables Configuration Variables}
The following configuration variables may be set as described in the {{!setvar} Setting Configuration Variables} section:

{ul
{- [remind_command]

      Determines the command used to execute Remind.
}
{- [reminders_file]

      Controls which Remind file (or Remind directory) Wyrd will operate on.
      The default is [~/.reminders] .
}
{- [edit_old_command]

      Controls the command used to edit a pre-existing reminder.
      The special strings ['%file%'] and ['%line%'] will be replaced with a filename to edit and a line number to navigate to within that file.
}
{- [edit_new_command]

      Controls the command used to edit a new reminder.
      The special character ['%file%'] will be replaced with a filename to edit.
      Ideally, this command should move the cursor to the last line of the file, where the new reminder template is created.
}
{- [edit_any_command]

      Controls the command used for editing a reminder file without selecting any particular reminder.
      The special character ['%file%'] will be replaced with a filename to edit.
}
{- [pager_command]

      Controls the command used for viewing keybindings (['?']), calendar, and reminders [{c, C, r, R}].
      Set this to ["less -c"] if you want [less(1)] to repaint the screen by clearing it instead of scrolling.
}
{- [default_zoom]

      Controls the default zoom level that Wyrd starts up with.
      This setting accepts an integer value of:
   {ul
      {- [0] for Hourly time increments}
      {- [1] for Half-Hourly time increments}
      {- [2] for Quarter-Hourly time increments.}}

      Any integer other than these 3 is invalid and will cause an error.
}
{- [timed_template]

      Controls the format of the [REM] line created when editing a new timed reminder.
      The following string substitutions will be made:
   {ul
      {- ['%monname%'] - month name}
      {- ['%mon%'] - month number (1-12)}
      {- ['%0mon%'] - month number (01-12)}
      {- ['%mday%'] - day of the month (1-31)}
      {- ['%0mday%'] - day of the month (01-31)}
      {- ['%year%'] - year}
      {- ['%hour%'] - hour}
      {- ['%min%'] - minute}
      {- ['%wdayname%'] - weekday name}
      {- ['%wday%'] - weekday number.}}

}
{- [untimed_template]

      Controls the format of the [REM] line created when editing a new untimed reminder.
      The substitution syntax is the same as for [timed_template].
}
{- [template]{e [N]}

      Controls the format of a generic user-defined [REM] line template; {e [N]} may range from 0 to 9.
      The substitution syntax is the same as for [timed_template].
}
{- [busy_algorithm]

      An integer value specifying which algorithm to use for measuring how busy the user is on a particular day.
      If [busy_algorithm="1"], then Wyrd will simply count the total number of reminders triggered on that day.
      If [busy_algorithm="2"], then Wyrd will count the number of hours of reminders that fall on that day.
      (Untimed reminders are assumed to occupy [untimed_duration] minutes.)
}
{- [untimed_duration]

      An integer value that specifies the assumed duration of an untimed reminder, in minutes.
      This is used only when computing the busy level with [busy_algorithm="2"].
}
{- [busy_level1]

      An integer value specifying the maximum number of reminders in a day (with [busy_algorithm="1"]) or maximum hours of reminders in a day (with [busy_algorithm="2"]) which will be colored using the color scheme for [calendar_level1].
}
{- [busy_level2]

      Same as above, using the [calendar_level2] color scheme.
}
{- [busy_level3]

      Same as above, using the [calendar_level2] color scheme rendered in bold.
}
{- [busy_level4]

      Same as above, using the [calendar_level3] color scheme.
      Any day with more reminders than this will be rendered using the [calendar_level3] color scheme rendered in bold.
}
{- [week_starts_monday]

      A boolean value (["true"] or ["false"]) that determines the first day of the week.
}
{- [schedule_12_hour]

      A boolean value that determines whether the timed reminders window is drawn using 12- or 24-hour time.
}
{- [selection_12_hour]

      A boolean value that determines whether the selection information is drawn with 12- or 24-hour time.
}
{- [status_12_hour]

      A boolean value that determines whether the current time is drawn using a 12- or 24-hour clock.
}
{- [description_12_hour]

      A boolean value that determines whether reminder start and end times are drawn using 12- or 24-hour time in the description window.
      This value also controls the format of timestamps in the formatted calendars produced by [view_week] and [view_month].
}
{- [center_cursor]

      A boolean value that determines how the screen and cursor move during scrolling operations.
      When set to ["true"], the cursor is fixed in the center of the timed reminders window, and the schedule scrolls around it.
      When set to ["false"] (the default), the cursor will move up and down the schedule during scrolling operations.
}
{- [goto_big_endian]

      A boolean value that determines how the [goto] operation will parse dates.
      When set to ["true"], date specifiers should be in ISO 8601 (YYYYMMDD) format.
      When set to ["false"], date specifiers should be in European style DDMMYYYY format.
}
{- [quick_date_US]

      A boolean value that determines how the [quick_add] operation will parse numeric dates with slashes, e.g. 6/1 (or 6/1/2006).
      When set to ["true"], the first number is a month and the second is the day of the month (June 1).
      When set to ["false"], these meanings of these two fields are switched (January 6).
}
{- [number_weeks]

      A boolean value that determines whether or not weeks should be numbered within the month calendar window.
      Weeks are numbered according to the ISO 8601 standard.
      The ISO standard week begins on Monday, so to avoid confusion it is recommended that [week_starts_monday] be set to ["true"] when week numbering is enabled.
}
{- [home_sticky]

      A boolean value that determines whether or not the cursor should "stick" to the "home" position.
      When this option is set to ["true"], then after pressing the [<home>] key the cursor will automatically follow the current date and time.
      The effect is cancelled by pressing any of the navigation keys.
}
{- [advance_warning]

      A boolean value that determines whether or not Wyrd should display advance warning of reminders.
      When set to ["true"], Wyrd will invoke Remind in a mode that generates advance warning of reminders as specified in the reminder file.
}
{- [untimed_window_width]

      An integer value that determines the target width of the month-calendar window and the untimed reminders window.
      The allowable range is 34 to ([$COLUMNS] - 40) characters, and Wyrd will silently disregard any setting outside this range.
}
{- [untimed_bold]

      A boolean value that determines whether or not Wyrd should render untimed reminders using a bold font.
}
{- [reminder_colors]

      Render reminders in color (auto/true/false) needs ccc support in termcap: [tput ccc && echo supported] auto will fallback to default colors.
}
}

For maximum usefulness, [busy_level1] < [busy_level2] < [busy_level3] < [busy_level4].


{1:operationslist Calendar Operations}

Every Wyrd operation can be made available to the interface using the syntax described in the section on {{!bindings} Creating Key Bindings}.

The following is a list of every available operation.

{ul
{- [scroll_up]

      move the cursor up one element}
{- [scroll_down]

      move the cursor down one element}
{- [next_day]

      jump ahead one day}
{- [previous_day]

      jump backward one day}
{- [next_week]

      jump ahead one week}
{- [previous_week]

      jump backward one week}
{- [next_month]

      jump ahead one month}
{- [previous_month]

      jump backward one month}
{- [home]

      jump to the current date and time}
{- [goto]

      begin entering a date specifier to jump to}
{- [zoom]

      zoom in on the day schedule view (this operation is cyclic)}
{- [edit]

      edit the selected reminder}
{- [edit_any]

      edit a reminder file, without selecting any particular reminder}
{- [scroll_description_up]

      scroll the description window contents up (when possible)}
{- [scroll_description_down]

      scroll the description window contents down (when possible)}
{- [quick_add]

      add a "quick reminder"}
{- [new_timed]

      create a new timed reminder}
{- [new_timed_dialog]

      same as previous, with a reminder file selection dialog}
{- [new_untimed]

      create a new untimed reminder}
{- [new_untimed_dialog]

      same as previous, with a reminder file selection dialog}
{- [new_template]{e [N]}

      create a new user-defined reminder using [templateN], where {e [N]} may range from 0 to 9}
{- [new_template]{e [N]}[_dialog]

      same as previous, with a reminder file selection dialog}
{- [copy]

      copy a reminder to Wyrd's clipboard}
{- [cut]

      delete a reminder and copy it to Wyrd's clipboard}
{- [paste]

      paste a reminder from Wyrd's clipboard into the schedule}
{- [paste_dialog]

      same as previous, with a reminder file selection dialog}
{- [switch_window]

      switch between the day schedule window on the left, and the untimed reminder window on the right}
{- [begin_search]

      begin entering a search string}
{- [search_next]

      search for the next occurrence of the search string}
{- [next_reminder]

      jump to the next reminder
      (The [next_reminder] operation locates reminders at the point they occur; if [advance_warning] is enabled, [next_reminder] will skip over any displayed warnings of an event.)
}
{- [view_remind]

      view the output of [remind] for the selected date}
{- [view_remind_all]

      view the output of [remind] for the selected date, triggering all non-expired reminders}
{- [view_week]

      view Remind's formatted calendar for the week that contains the selected date (the in-calendar timestamp formats are determined by the value of [description_12_hour])}
{- [view_month]

      view Remind's formatted calendar for the month that contains the selected date (the in-calendar timestamp formats are determined by the value of [description_12_hour])}
{- [refresh]

      refresh the display}
{- [quit]

      exit Wyrd}
{- [entry_complete]

      signal completion of search string entry or date specifier}
{- [entry_backspace]

      delete the last character of the search string or date specifier}
{- [entry_cancel]

      cancel entry of a search string or date specifier}}


{1:colorable_objects Colorable Objects}

Each of Wyrd's on-screen elements may be colored by the color scheme of your choice, using the syntax defined in the section on {{!colors} Setting the Color Scheme}.

The following is a list of all colorable objects.

{ul
{- [help]

      the help bar at the top of the screen}
{- [timed_default]

      an empty timeslot in the day-schedule window}
{- [timed_current]

      the current time in the day-schedule window (if it is visible)}
{- [timed_reminder1]

      a nonempty timeslot in the day-schedule window, indented to level 1}
{- [timed_reminder2]

      a nonempty timeslot in the day-schedule window, indented to level 2}
{- [timed_reminder3]

      a nonempty timeslot in the day-schedule window, indented to level 3}
{- [timed_reminder4]

      a nonempty timeslot in the day-schedule window, indented to level 4}
{- [untimed_reminder]

      an entry in the untimed reminders window}
{- [timed_date]

      the vertical date strip at the left side of the screen}
{- [selection_info]

      the line providing date/time for the current selection}
{- [description]

      the reminder description window}
{- [status]

      the bottom bar providing current date and time}
{- [calendar_labels]

      the month and weekday labels in the calendar window}
{- [calendar_level1]

      calendar days with low activity}
{- [calendar_level2]

      calendar days with medium activity}
{- [calendar_level3]

      calendar days with high activity}
{- [calendar_today]

      the current day in the calendar window (if it is visible)}
{- [left_divider]

      the vertical line to the left of the timed reminders window}
{- [right_divider]

      the vertical and horizontal lines to the right of the timed reminders window}}
